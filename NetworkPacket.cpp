/**
  * Date:   14.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  NetworkPacket class implementation file
  **/

#include "NetworkPacket.h"

#include "Utility.h"

std::istream& NetworkPacket::read(std::istream& is)
{
    if (is.fail())
    {
        return is;
    }

    // read pcap record header
    is.read((char *) &m_pcapRecHdr, sizeof(NetworkPacket::PcapRecHeader));
    if (is.fail())
    {
        return is;
    }
    // swap endiannes if host is little endian
    if (m_swapPcapRecHdr == true) {
        Utility::swapEndianness32bit(&m_pcapRecHdr.ts_sec);
        Utility::swapEndianness32bit(&m_pcapRecHdr.ts_usec);
        Utility::swapEndianness32bit(&m_pcapRecHdr.incl_len);
        Utility::swapEndianness32bit(&m_pcapRecHdr.orig_len);
    }

    // read ethernet frame header
    is.read((char *) &m_ethHdr, sizeof(NetworkPacket::EthernetHeader));
    if (is.fail())
    {
        return is;
    }
    // swap endiannes if host is little endian
    if (Utility::checkEndianness() == Utility::Endianness::ENDIAN_LITTLE) {
        Utility::swapEndianness16bit(&m_ethHdr.ether_type);
    }

    if (m_ethHdr.ether_type != 0x0800) {
        // skip packet
        return is.seekg(m_pcapRecHdr.incl_len - (uint32_t) sizeof(NetworkPacket::EthernetHeader),
                        std::ios_base::cur);
    }

    // read ipv4 header
    is.read((char *) &m_ipv4Hdr, sizeof(NetworkPacket::Ipv4Header));
    if (is.fail())
    {
        return is;
    }
    // swap endiannes if host is little endian
    if (Utility::checkEndianness() == Utility::Endianness::ENDIAN_LITTLE) {
        Utility::swapEndianness16bit(&m_ipv4Hdr.length);
        Utility::swapEndianness16bit(&m_ipv4Hdr.identificaiton);
        Utility::swapEndianness16bit(&m_ipv4Hdr.flags_offset);
        Utility::swapEndianness16bit(&m_ipv4Hdr.checksum);
        Utility::swapEndianness32bit(&m_ipv4Hdr.source);
        Utility::swapEndianness32bit(&m_ipv4Hdr.destination);
    }
    if (m_ipv4Hdr.protocol != 6) {
        // skip packet
        return is.seekg(m_pcapRecHdr.incl_len - (uint32_t) sizeof(NetworkPacket::EthernetHeader) - 20,
                        std::ios_base::cur);
    }

    // read tcp header
     is.read((char *) &m_tcpHdr, sizeof(NetworkPacket::TcpHeader));
     if (is.fail())
     {
         return is;
     }
    // swap endiannes if host is little endian
    if (Utility::checkEndianness() == Utility::Endianness::ENDIAN_LITTLE) {
         Utility::swapEndianness16bit(&m_tcpHdr.source);
         Utility::swapEndianness16bit(&m_tcpHdr.destination);
         Utility::swapEndianness32bit(&m_tcpHdr.sequence);
         Utility::swapEndianness32bit(&m_tcpHdr.acknowledgment);
         Utility::swapEndianness16bit(&m_tcpHdr.offset_reserved_flags);
         Utility::swapEndianness16bit(&m_tcpHdr.win_size);
         Utility::swapEndianness16bit(&m_tcpHdr.checksum);
         Utility::swapEndianness16bit(&m_tcpHdr.urgent);
    }

    // we have all data we need right now, set file position to next packet and
    // process current one
    size_t skipSize =
            (size_t) m_pcapRecHdr.incl_len -            // full packet length
            sizeof (NetworkPacket::EthernetHeader) -    // ethernet header length
            (size_t) (m_ipv4Hdr.ver_ihl & 0x0F) * 4 -                // ipv4 header length
            sizeof (NetworkPacket::TcpHeader);          // tcp header length w/o options

    return is.seekg(skipSize, std::ios_base::cur);
}

uint16_t NetworkPacket::getTcpFlags() const
{
    return (uint16_t) (m_tcpHdr.offset_reserved_flags & 0x1FF);
}
