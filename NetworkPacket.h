/**
  * Date:   14.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  NetworkPacket class header file.
  **/

#ifndef NETWORKPACKET_H
#define NETWORKPACKET_H

#include <istream>
#include <cstdint>

class NetworkPacket
{
public:
    /*
     * pcap packet header
     */
    struct PcapRecHeader {
        uint32_t ts_sec;         /* timestamp seconds */
        uint32_t ts_usec;        /* timestamp microseconds */
        uint32_t incl_len;       /* number of octets of packet saved in file */
        uint32_t orig_len;       /* actual length of packet */
    };

    /*
     * Ethernet data-link layer frame
     */
    struct EthernetHeader
    {
        uint8_t destination[6]; /* Destination MAC */
        uint8_t source[6];      /* Source MAC */
        uint16_t ether_type;    /* EtherType */
    };

    /*
     * IPv4 Header
     */
    struct Ipv4Header
    {
        uint8_t     ver_ihl;        /* 4bit version and 4bit internet header length */
        uint8_t     dscp_ecn;       /* 6bit Differentiated Services Code Point */
                                    /* 2bit Explicit Congestion Notification */
        uint16_t    length;         /* Total Length */
        uint16_t    identificaiton;
        uint16_t    flags_offset;   /* 3bit flags, 13bit offset */
        uint8_t     ttl;            /* Time To Live */
        uint8_t     protocol;
        uint16_t    checksum;       /* Header Checksum */
        uint32_t    source;         /* Source address */
        uint32_t    destination;    /* Destination address */
    };

    /*
     * TCP Header
     */
    struct TcpHeader
    {
        uint16_t    source;                 /* Source port */
        uint16_t    destination;            /* Destination port */
        uint32_t    sequence;               /* Sequence number */
        uint32_t    acknowledgment;         /* Acknowledgment number */
        uint16_t    offset_reserved_flags;  /* 4bits Data offset, 3bits Reserved, 9bits flag */
        uint16_t    win_size;               /* Window size */
        uint16_t    checksum;               /* Checksum */
        uint16_t    urgent;                 /* Urgent pointer */
    };

    std::istream &read(std::istream &is);

    NetworkPacket(bool swapPcapRecHdr = false) : m_swapPcapRecHdr(swapPcapRecHdr) {}
    ~NetworkPacket() = default;

    uint16_t getTcpFlags(void) const;
    const Ipv4Header& getIpv4Hdr(void) const { return m_ipv4Hdr; }
    const TcpHeader& getTcpHdr(void) const { return m_tcpHdr; }

private:
    PcapRecHeader m_pcapRecHdr;
    EthernetHeader m_ethHdr;
    Ipv4Header  m_ipv4Hdr;
    TcpHeader   m_tcpHdr;
    bool        m_swapPcapRecHdr;
};

#endif // NETWORKPACKET_H
