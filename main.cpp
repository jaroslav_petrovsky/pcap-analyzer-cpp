/**
  * Date:  147.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  Your task is to  develop a C/C++ program that has a path to the pcap
  *         file on input and outputs the number of TCP connections that reach
  *         established state.  In the first phase of the project we assume that
  *         our honeypot processes  only IPv4 packets and the packets are not
  *         fragmented.
  **/

#include "PcapAnalyzer.h"
#include "TcpSessionAnalyzator.h"

#include <memory>
#include <iostream>

int main(int argc, char *argv[])
{
    // create packet analyzator
    std::shared_ptr<TcpSessionAnalyzator> tcpSessionAnalyzator(new TcpSessionAnalyzator());

    // load application
    PcapAnalyzer::getInstance().addPacketAnalyzator(tcpSessionAnalyzator);
    PcapAnalyzer::getInstance().analyzePcap(argv[1]);
    PcapAnalyzer::getInstance().removePacketAnalyzator(tcpSessionAnalyzator);

    // print results
    std::cout << tcpSessionAnalyzator->getSessionCounter() << "\n" << std::endl;
}
