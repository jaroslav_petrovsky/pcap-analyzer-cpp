/**
  * Date:   14.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  TcpSessionAnalyzator counts established sessions
  **/

#ifndef TCPSESSIONANALYZATOR_H
#define TCPSESSIONANALYZATOR_H

#include "PacketAnalyzator.h"

#include <stdint.h>
#include <vector>

class TcpSessionAnalyzator : public PacketAnalyzator
{
public:
    /*
     * Struct for representing TCP session
     */
    struct TcpSession
    {
        uint32_t clientAddr;
        uint32_t serverAddr;
        uint16_t clientPort;
        uint16_t serverPort;
    };

    TcpSessionAnalyzator() : m_sessionCounter(0) {}
    ~TcpSessionAnalyzator() = default;

    void analyzePacket(const NetworkPacket* packet);
    int getSessionCounter() {return m_sessionCounter; }

private:
    void addSession(TcpSession session);

private:
    std::vector<TcpSession> m_sessions;
    int m_sessionCounter;
};

#endif // TCPSESSIONANALYZATOR_H
