cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_FLAGS "-Wall -Wextra -Wstrict-prototypes -Wshadow -std=c++11")
project(pcap-analyzer-cpp)
add_executable(${PROJECT_NAME}
    "main.cpp"
    "PcapAnalyzer.cpp"
    "NetworkPacket.cpp"
    "Utility.cpp"
    "TcpSessionAnalyzator")
