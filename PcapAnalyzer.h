/**
  * Date:   14.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  Application singleton header file
  **/

#ifndef PCAPANALYZER_H
#define PCAPANALYZER_H

#include "PacketAnalyzator.h"

#include <string>
#include <vector>
#include <memory>

class PcapAnalyzer
{
public:
    // header struct
    struct PcapHeader {
        uint32_t magic_number;   /* magic number */
        uint16_t version_major;  /* major version number */
        uint16_t version_minor;  /* minor version number */
        int32_t  thiszone;       /* GMT to local correction */
        uint32_t sigfigs;        /* accuracy of timestamps */
        uint32_t snaplen;        /* max length of captured packets, in octets */
        uint32_t network;        /* data link type */
    };

    PcapAnalyzer(const PcapAnalyzer&) = delete;
    void operator=(const PcapAnalyzer&) = delete;

    static PcapAnalyzer& getInstance()
    {
        static PcapAnalyzer instance;
        return instance;
    }

    void analyzePcap(const std::string& fileName);

    void addPacketAnalyzator(std::shared_ptr<PacketAnalyzator> packetAnalyzator);
    void removePacketAnalyzator(std::shared_ptr<PacketAnalyzator> packetAnalyzator);

private:
    PcapAnalyzer() {}

    PcapHeader m_pcapHeader;
    std::vector<std::shared_ptr<PacketAnalyzator>> m_analyzators;
};

#endif // PCAPANALYZER_H

