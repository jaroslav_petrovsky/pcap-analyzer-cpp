/**
  * Date:   14.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  TcpSessionAnalyzator counts established sessions
  **/

#include "TcpSessionAnalyzator.h"
#include "NetworkPacket.h"

#include <unordered_set>
#include <algorithm>
#include <functional>

std::function<bool (const TcpSessionAnalyzator::TcpSession& session1, const TcpSessionAnalyzator::TcpSession& session2)> sessionCompare =
        [] (const TcpSessionAnalyzator::TcpSession& session1,const TcpSessionAnalyzator::TcpSession& session2) {
           if( (session1.clientAddr == session2.clientAddr) &&
                   (session1.serverAddr == session2.serverAddr) &&
                   (session1.clientPort == session2.clientPort) &&
                   (session1.serverPort == session2.serverPort))
           {
               return true;
           }

           return false;
       };

void TcpSessionAnalyzator::analyzePacket(const NetworkPacket *packet)
{
    // check packet for SYN-ACK flags
    if ((packet->getTcpFlags() & 0x12) == 0x12)
    {
        TcpSessionAnalyzator::TcpSession session = {
            packet->getIpv4Hdr().destination,
            packet->getIpv4Hdr().source,
            packet->getTcpHdr().destination,
            packet->getTcpHdr().source
        };

        this->addSession(session);
        return;
    }

    // check packet for ACK flag
    if ((packet->getTcpFlags() & 0x10) == 0x10)
    {
        TcpSessionAnalyzator::TcpSession session = {
            packet->getIpv4Hdr().source,
            packet->getIpv4Hdr().destination,
            packet->getTcpHdr().source,
            packet->getTcpHdr().destination
        };

        // find and remove connection
        auto found = std::find_if(m_sessions.begin(), m_sessions.end(), std::bind(sessionCompare, session, std::placeholders::_1));

        if (found != m_sessions.end()) {
            m_sessions.erase(found);
            ++m_sessionCounter;
            return;
        }
    }

    return;
}

void TcpSessionAnalyzator::addSession(TcpSessionAnalyzator::TcpSession session)
{
    auto found = std::find_if(m_sessions.begin(), m_sessions.end(), std::bind(sessionCompare, session, std::placeholders::_1));

    if (found != m_sessions.end()) {
        return;
    }

    m_sessions.push_back(session);
}
