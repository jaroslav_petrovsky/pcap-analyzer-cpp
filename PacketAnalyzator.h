/**
  * Date:   07.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  Common ancestor class from packet analysis classes
  **/

#ifndef PACKETANALYZATOR_H
#define PACKETANALYZATOR_H

class NetworkPacket;

class PacketAnalyzator
{
public:
    PacketAnalyzator() = default;
    virtual ~PacketAnalyzator() = default;

    virtual void analyzePacket(const NetworkPacket* packet) = 0;
};

#endif // PACKETANALYZATOR_H
