/**
  * Date:   14.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  Utility functions
  **/

#include <stdint.h>

namespace Utility {

enum class Endianness {
    ENDIAN_BIG,
    ENDIAN_LITTLE
};

void swapEndianness16bit(uint16_t* data)
{
    *data = (uint16_t) (*data >> 8) | (uint16_t) (*data << 8);
}

void swapEndianness32bit(uint32_t* data)
{
    *data = ((*data >> 24) & 0xff) |        // move byte 3 to byte 0
            ((*data << 8) & 0xff0000) |     // move byte 1 to byte 2
            ((*data >> 8) & 0xff00) |       // move byte 2 to byte 1
            ((*data << 24) & 0xff000000);   // byte 0 to byte 3
}

Endianness checkEndianness()
{
    unsigned int num = 1;
    return (Endianness) (*(char *)&num);
}

}
