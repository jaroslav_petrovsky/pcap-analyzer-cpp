/**
  * Date:   14.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  Application singleton implementation file
  **/

#include "PcapAnalyzer.h"

#include "NetworkPacket.h"
#include "Utility.h"

#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>

std::istream& operator>>(std::istream& is, NetworkPacket& packet)
{
    return packet.read(is);
}

void PcapAnalyzer::analyzePcap(const std::string &fileName)
{
    // open file for reading
    std::ifstream pcap;
    pcap.open(fileName, std::ios::in | std::ios::binary);

    // read global pcap header
    if (pcap.fail()) {
        return;
    }

    // read data
    pcap.read((char*) (&this->m_pcapHeader), sizeof(PcapAnalyzer::PcapHeader));

    // correct byte order
    if (m_pcapHeader.magic_number == 0xd4c3b2a1 ||
            m_pcapHeader.magic_number == 0x4d3cb2a1) {
        Utility::swapEndianness16bit(&m_pcapHeader.version_major);
        Utility::swapEndianness16bit(&m_pcapHeader.version_minor);
        Utility::swapEndianness32bit((uint32_t*) &m_pcapHeader.thiszone);
        Utility::swapEndianness32bit(&m_pcapHeader.sigfigs);
        Utility::swapEndianness32bit(&m_pcapHeader.snaplen);
        Utility::swapEndianness32bit(&m_pcapHeader.network);
    }

    // allow only ethernet packets
    if (m_pcapHeader.network != 1) {
        return;
    }

    // analyze each packet
    NetworkPacket packet(m_pcapHeader.magic_number == 0xd4c3b2a1 || m_pcapHeader.magic_number == 0x4d3cb2a1);
    while (!pcap.eof()) {
        pcap >> packet;

        for(std::shared_ptr<PacketAnalyzator> analyzator : m_analyzators) {
            analyzator->analyzePacket(&packet);
        }
    }
}

void PcapAnalyzer::addPacketAnalyzator(std::shared_ptr<PacketAnalyzator> packetAnalyzator)
{
    m_analyzators.push_back(packetAnalyzator);
}

void PcapAnalyzer::removePacketAnalyzator(std::shared_ptr<PacketAnalyzator> packetAnalyzator)
{
    m_analyzators.erase(std::find(m_analyzators.begin(), m_analyzators.end(), packetAnalyzator));
}
