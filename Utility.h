/**
  * Date:   14.02.2017
  * Author: Jaroslav Petrovsky
  * Brief:  Utility functions
  **/

#ifndef UTILITY_H
#define UTILITY_H

namespace Utility {

enum class Endianness {
    ENDIAN_BIG,
    ENDIAN_LITTLE
};

void swapEndianness16bit(uint16_t* data);
void swapEndianness32bit(uint32_t* data);
Endianness checkEndianness();

}

#endif // UTILITY_H
